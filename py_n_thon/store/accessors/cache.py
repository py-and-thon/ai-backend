import datetime
import os
from typing import List, Dict, Any

import sqlalchemy as sa
from asyncpgsa.connection import SAConnection
from joblib import Parallel, delayed
from surprise import SVD, dump

from py_n_thon.store.accessors.base import BaseEntityAccessor
from py_n_thon.store.accessors.pg import with_pg_conn
from py_n_thon.store.models import events_table
from py_n_thon.store.store import Store


class CacheAccessor(BaseEntityAccessor):
    SVD_FILE_PATH = os.path.join(os.path.dirname(__file__), '.svd.model')

    def __init__(self, store: Store):
        self.store = store

    async def connect(self):
        pass

    async def disconnect(self):
        pass

    async def check_svd_in_cache(self) -> None:
        return os.path.exists(self.SVD_FILE_PATH)

    async def get_cached_svd(self) -> SVD:
        _, loaded_algo = dump.load(self.SVD_FILE_PATH)
        return loaded_algo

    async def cache_svd(self, svd: SVD) -> None:
        dump.dump(self.SVD_FILE_PATH, algo=svd)

    # async def check_recommendation_in_cache(self, sirius_user_id: int) -> None:
    #     raise NotImplementedError
    #
    # async def get_cached_recommendation(self, sirius_user_id: int) -> Dict[str, List[Dict[str, Any]]]:
    #     raise NotImplementedError
    #
    # async def cache_recommendation(self, sirius_user_id: int, results: Dict[str, List[Dict[str, Any]]]) -> None:
    #     raise NotImplementedError
