import os

from py_n_thon.store.accessors.base import BaseAccessor
from py_n_thon.store.config import Config
from py_n_thon.store.store import Store


class StringCompareModelFileAccessor(BaseAccessor):
    def __init__(self, store: Store, config: Config):
        self.store = store
        self._models_path = config.models_path

    async def connect(self):
        pass

    async def disconnect(self):
        pass

    def get_word2vec_model_path(self) -> str:
        return os.path.join(self._models_path, 'taiga_upos_skipgram_300_2_2018.vec')
