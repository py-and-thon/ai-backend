from collections import defaultdict
from typing import Dict, List, Tuple, Any

import sqlalchemy as sa
from asyncpgsa.connection import SAConnection

from py_n_thon.store.accessors.base import BaseEntityAccessor
from py_n_thon.store.accessors.pg import with_pg_conn
from py_n_thon.store.models import achievements_table, history_table, categories_table, UserData, events_table, \
    ItemData, cities_table
from py_n_thon.store.store import Store


class ItemDataAccessor(BaseEntityAccessor):
    def __init__(self, store: Store):
        self.store = store

    async def connect(self):
        pass

    async def disconnect(self):
        pass

    @with_pg_conn
    async def fetch_one(self, event_id: int, conn: SAConnection) -> ItemData:
        query = sa.select([events_table.c.id, sa.alias(categories_table.c.name, 'category_title')]). \
            select_from(sa.join(events_table, categories_table, events_table.c.category == categories_table.c.id)). \
            where(events_table.c.id == event_id)
        row = await conn.fetchrow(query)
        return ItemData(row[0], row[1])

    @with_pg_conn
    async def fetch_all(self, conn: SAConnection) -> List[ItemData]:
        query = sa.select([events_table.c.id, sa.alias(categories_table.c.name, 'category_title')]). \
            select_from(sa.join(events_table, categories_table, events_table.c.category == categories_table.c.id))
        return [ItemData(row[0], row[1]) for row in await conn.fetch(query)]

    @with_pg_conn
    async def convert_item_data_to_full(self, item_data: List[ItemData], conn: SAConnection) -> List[Dict[str, Any]]:
        join_query = sa.\
            join(events_table, categories_table, events_table.c.category == categories_table.c.id). \
            join(events_table, cities_table, events_table.c.city_id == cities_table.c.id)
        query = sa.select([events_table, sa.alias(categories_table.c.name, 'category_title'), sa.alias(cities_table.c.name, 'city_title')]). \
            select_from(join_query).\
            where(events_table.c.id.in_([it.id_ for it in item_data]))
        return await conn.fetch(query)


