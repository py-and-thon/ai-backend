import logging
from typing import List, Union, Dict

import gensim
from pymystem3 import Mystem

from py_n_thon.store.accessors.base import BaseAccessor
from py_n_thon.store.store import Store

_logger = logging.getLogger(__name__)


class StringCompareAccessor(BaseAccessor):
    def __init__(self, store: Store):
        self.store = store
        self._score_models: _ScoreModels = None

    async def connect(self):
        self._score_models = _ScoreModels(self.store)

    async def disconnect(self):
        pass

    def compare(self, haystack: str, needle: str) -> float:
        result_score = 0

        haystack_transformed = self._tag_string(haystack)
        needle_transformed = self._tag_string(needle)

        if len(haystack_transformed) == 0:
            return result_score
        if len(needle_transformed) == 0:
            return result_score

        w2v_model = self._score_models.word2vec

        for needle_transformed_word in needle_transformed:
            if needle_transformed_word not in w2v_model.vocab:
                continue

            for haystack_transformed_word in haystack_transformed:
                if haystack_transformed_word not in w2v_model.vocab:
                    continue
                word_similarity = w2v_model.similarity(haystack_transformed_word, needle_transformed)
                result_score += word_similarity

            result_score /= len(haystack_transformed)

        result_score /= len(needle_transformed)

        return result_score

    def _tag_string(self, text: str) -> List[str]:
        processed = self._score_models.mystem.analyze(text)
        tagged = []
        for w in processed:
            try:
                lemma = w['analysis'][0]['lex'].lower().strip()
                pos = w['analysis'][0]['gr'].split(',')[0]
                pos = pos.split('=')[0].strip()
                if pos in self._score_models.upos_mystem_mapping:
                    tagged.append(lemma + '_' + self._score_models.upos_mystem_mapping[pos])
                else:
                    tagged.append(lemma + '_X')
            except (KeyError, IndexError):
                continue
        return tagged


class _ScoreModels:
    def __init__(self, store: Store):
        self.store = store

        _logger.info('Loading word2vec model')
        self.word2vec = self._load_word2vec_model()

        _logger.info('Loading mystem')
        self.mystem = Mystem()

        _logger.info('Done')

    def _load_word2vec_model(self) -> Union[gensim.models.KeyedVectors, gensim.models.Word2Vec]:
        path = self.store.string_compare_model_file.get_word2vec_model_path()
        if path.endswith('.vec.gz') or path.endswith('.vec'):
            model = gensim.models.KeyedVectors.load_word2vec_format(path, binary=False)
        elif path.endswith('.bin.gz') or path.endswith('.bin'):
            model = gensim.models.KeyedVectors.load_word2vec_format(path, binary=True)
        else:
            model = gensim.models.Word2Vec.load(path)
        model.init_sims(replace=True)
        return model

    @property
    def upos_mystem_mapping(self) -> Dict[str, str]:
        return {'COM': 'ADJ', 'APRO': 'DET', 'PART': 'PART', 'PR': 'ADP', 'ADV': 'ADV', 'INTJ': 'INTJ', 'S': 'NOUN',
                'V': 'VERB', 'CONJ': 'SCONJ', 'UNKN': 'X', 'ANUM': 'ADJ', 'NUM': 'NUM', 'NONLEX': 'X', 'SPRO': 'PRON',
                'ADVPRO': 'ADV', 'A': 'ADJ'}
