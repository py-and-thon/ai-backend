from typing import List

import pandas as pd
from surprise import Dataset, Reader, SVD

from py_n_thon.store.accessors.base import BaseEntityAccessor
from py_n_thon.store.models import UserData, ItemData
from py_n_thon.store.store import Store


class RecommendAccessor(BaseEntityAccessor):
    def __init__(self, store: Store):
        self.store = store

    async def connect(self):
        pass

    async def disconnect(self):
        pass

    @staticmethod
    def build_svd(user_data: List[UserData], item_data: List[ItemData]) -> SVD:
        df = pd.DataFrame([(uit.sirius_user_id, iit.id_, uit.categories_scores.get(iit.category, 0))
                           for iit in item_data for uit in user_data])
        ds = Dataset.load_from_df(df, Reader(rating_scale=(0, 1)))
        ts = ds.build_full_trainset().build_testset()

        svd = SVD()
        svd.fit(ts)
        return svd

    @staticmethod
    def recommend(user_data: UserData, item_data: List[ItemData], svd: SVD) -> List[ItemData]:
        item_data_ratings = list()
        for it in item_data:
            _, _, _, pred_rating, _ = svd.predict(user_data.sirius_user_id, it.id_)
            item_data_ratings.append(pred_rating)
        return sorted(range(len(item_data)), key=lambda i: item_data_ratings[i], reverse=True)
