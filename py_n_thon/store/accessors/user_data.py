from collections import defaultdict
from typing import Dict, List, Tuple

import sqlalchemy as sa
from asyncpgsa.connection import SAConnection

from py_n_thon.store.accessors.base import BaseEntityAccessor
from py_n_thon.store.accessors.pg import with_pg_conn
from py_n_thon.store.models import achievements_table, history_table, categories_table, UserData
from py_n_thon.store.store import Store


class UserDataAccessor(BaseEntityAccessor):
    def __init__(self, store: Store):
        self.store = store

    async def connect(self):
        pass

    async def disconnect(self):
        pass

    @with_pg_conn
    async def fetch_one(self, sirius_user_id: int,
                        use_achievements: bool, use_history: bool, use_vk: bool,
                        conn: SAConnection) -> UserData:
        assert any([use_achievements, use_history, use_vk])

        categories_scores: Dict[str, float] = defaultdict(lambda: 0)
        weights_sum = 0
        total_count = 0

        if use_achievements:
            query = sa.select([achievements_table.c.category_text]).where(achievements_table.userid == sirius_user_id)
            async for row in conn.cursor(query):
                categories_scores[row['category_text']] += 5
                total_count += 1
            weights_sum += 5

        if use_history:
            query = sa.select([sa.alias(categories_table.c.name, 'category_title')]).\
                select_from(sa.join(history_table, categories_table, history_table.c.category == categories_table.c.id)).\
                where(history_table.c.sirius_user_id)
            async for row in conn.cursor(query):
                categories_scores[row['category_title']] += 1
                total_count += 1
            weights_sum += 1

        for k in categories_scores.keys():
            categories_scores[k] /= total_count
            categories_scores[k] /= weights_sum

        return UserData(sirius_user_id, categories_scores)

    @with_pg_conn
    async def fetch_all(self, use_achievements: bool, use_history: bool, use_vk: bool,
                        conn: SAConnection) -> List[UserData]:
        result = list()
        for sirius_user_id in await self.list_all_user_ids(conn):
            result.append(await self.fetch_one(sirius_user_id, use_achievements, use_history, use_vk))
        return result

    @with_pg_conn
    async def list_all_user_ids(self, conn: SAConnection) -> List[int]:
        query = sa.select([achievements_table.c.id])
        return [it[0] for it in await conn.fetch(query)]
