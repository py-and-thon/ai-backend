from typing import Callable

import asyncpgsa
from asyncpg.pool import Pool

from py_n_thon.store.accessors.base import BaseAccessor
from py_n_thon.store.config import Config
from py_n_thon.store.store import Store


class PgAccessor(BaseAccessor):
    def __init__(self, store: Store, config: Config):
        self.store = store
        self._database_url = config.database_url
        self._pool = None

    async def connect(self) -> None:
        self._pool = await asyncpgsa.create_pool(dsn=self._database_url, min_size=1, max_size=1)

    async def disconnect(self):
        pass

    @property
    def pool(self) -> Pool:
        return self._pool


def with_pg_conn(func: Callable):
    async def wrapper(self, *args, **kwargs):
        do_make_conn = kwargs.get('conn') is None
        if do_make_conn:
            kwargs['conn'] = await self.store.pg.pool.acquire()

        try:
            return await func(self, *args, **kwargs)
        finally:
            if do_make_conn:
                await self.store.pg.pool.release(kwargs.get('conn'))

    return wrapper

