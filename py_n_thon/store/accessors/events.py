import logging
import random
from collections import Counter
from typing import List, Dict, Any

from asyncpgsa.connection import SAConnection

from py_n_thon.store.accessors.base import BaseEntityAccessor
from py_n_thon.store.accessors.pg import with_pg_conn
from py_n_thon.store.store import Store

_logger = logging.getLogger(__name__)

logging.basicConfig(level=logging.INFO)


class EventsAccessor(BaseEntityAccessor):
    def __init__(self, store: Store):
        self.store = store

    async def connect(self):
        pass

    async def disconnect(self):
        pass

    @with_pg_conn
    async def search(self, search_query: str, sirius_user_id: str, conn: SAConnection) -> List[Dict[str, Any]]:
        return await self.store.search.search(search_query, sirius_user_id, conn=conn)

    @with_pg_conn
    async def list(self, sirius_user_id: str, conn: SAConnection) -> Dict[str, List[Dict[str, Any]]]:
        # if await self.store.cache.check_recommendation_in_cache(sirius_user_id):
        #     return await self.store.cache.get_cached_recommendation(sirius_user_id)

        _logger.info('user_data.fetch_one')
        current_user_data = await self.store.user_data.fetch_one(sirius_user_id, sirius_user_id,
                                                                 use_achievements=True, use_history=True)

        _logger.info('item_data.fetch_all')
        all_item_data = await self.store.item_data.fetch_all(conn=conn)

        if await self.store.cache.check_svd_in_cache():
            _logger.info('got svd from cache')
            svd = await self.store.cache.get_cached_svd(sirius_user_id)
        else:
            _logger.info('making new svd: user_data.fetch_all')
            all_user_data = await self.store.user_data.fetch_all(conn=conn)
            _logger.info('making new svd: svd')
            svd = await self.store.recommend.build_svd(all_user_data, all_item_data)
            await self.store.cache.cache_svd(svd)

        _logger.info('recommending')
        sorted_recommended_items = self.store.recommend.recommend(current_user_data, all_item_data, svd)
        _logger.info('recommended')

        best_categories = Counter()
        for it in sorted_recommended_items:
            best_categories[it.category] += 1
        best_categories = [it[0] for it in best_categories.most_common(3)]

        sections = {
            'Исходя из ваших достижений': sorted_recommended_items,
        }
        for it in best_categories:
            sections['Так как вам нравится ' + it.lower()] = [it1 for it1 in sorted_recommended_items
                                                              if it1.category == it]

        for k in sections.keys():
            count = min(7, len(sections[k]))
            if count > 0:
                sections[k][0:count] = random.sample(sections[k][0:count], count)

        for k in sections.keys():
            sections[k] = self.store.item_data.convert_item_data_to_full(sections[k])
        _logger.info('done')

        # await self.store.cache.cache_recommendation(sirius_user_id, sections)
        return sections

    @with_pg_conn
    async def list_tips(self, sirius_user_id: str, conn: SAConnection) -> List[str]:
        raise NotImplementedError
