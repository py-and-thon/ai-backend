import datetime
from dataclasses import dataclass
from typing import List, Dict, Any

import sqlalchemy as sa
from asyncpgsa.connection import SAConnection
from joblib import Parallel, delayed

from py_n_thon.store.accessors.base import BaseEntityAccessor
from py_n_thon.store.accessors.pg import with_pg_conn
from py_n_thon.store.accessors.string_compare import StringCompareAccessor
from py_n_thon.store.accessors.string_compare_model_file import StringCompareModelFileAccessor
from py_n_thon.store.models import events_table
from py_n_thon.store.store import Store


class SearchAccessor(BaseEntityAccessor):
    def __init__(self, store: Store):
        self.store = store

        @dataclass
        class Ministore:
            string_compare: StringCompareAccessor
            string_compare_model_file: StringCompareModelFileAccessor
        self._ministore = Ministore(self.store.string_compare, self.store.string_compare_model_file)

    async def connect(self):
        pass

    async def disconnect(self):
        pass

    @with_pg_conn
    async def search(self, search_query: str, sirius_user_id: str, conn: SAConnection) -> List[Dict[str, Any]]:
        query = sa.select([events_table])
        events = await conn.fetch(query)

        def score_event(ministore, event: Dict[str, Any]) -> float:
            return ministore.string_compare.compare(f'{event["name"]} {event["description"]}', search_query)

        # events_scores = Parallel(n_jobs=-1)(delayed(score_event)(self._ministore, it) for it in events)
        events_scores = [score_event(self._ministore, it) for it in events]

        events_sort_indexes = sorted(range(len(events_scores)), key=events_scores.__getitem__, reverse=True)
        sorted_events = [events[i] for i in events_sort_indexes]
        return sorted_events
