from abc import abstractmethod, ABCMeta


class BaseAccessor(metaclass=ABCMeta):
    @abstractmethod
    async def connect(self):
        raise NotImplementedError

    @abstractmethod
    async def disconnect(self):
        raise NotImplementedError


class BaseEntityAccessor(BaseAccessor):
    async def connect(self):
        pass

    async def disconnect(self):
        pass
