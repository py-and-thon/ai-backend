import aioredis
from asyncpg.pool import Pool

from py_n_thon.store.accessors.base import BaseAccessor
from py_n_thon.store.config import Config
from py_n_thon.store.store import Store


class RedisAccessor(BaseAccessor):
    def __init__(self, store: Store, config: Config):
        self.store = store
        self._database_url = config.redis_url
        self._pool = None

    async def connect(self) -> None:
        self._pool = await aioredis.create_redis_pool(self._database_url, minsize=1, maxsize=1)

    async def disconnect(self):
        self._pool.close()

    @property
    def pool(self) -> Pool:
        return self._pool

