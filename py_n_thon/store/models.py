from dataclasses import dataclass
from typing import Dict

import sqlalchemy as sa

metadata = sa.MetaData()

events_table = sa.Table('events', metadata,
                        sa.Column('id', sa.Integer()),
                        sa.Column('name', sa.String()),
                        sa.Column('description', sa.Text()),
                        sa.Column('city_id', sa.Integer()),
                        sa.Column('category_id', sa.Integer()),
                        sa.Column('image_url', sa.String()),
                        sa.Column('rate', sa.Float()))

achievements_table = sa.Table('sirius_dataset_2', metadata,
                              sa.Column('id', sa.Integer()),
                              sa.Column('userid', sa.Integer()),
                              sa.Column('achievement', sa.String()),
                              sa.Column('category_text', sa.String()),
                              sa.Column('school', sa.String()),
                              sa.Column('school_class', sa.Float()),
                              sa.Column('region_text', sa.String()),
                              sa.Column('achievement_type', sa.String()),
                              sa.Column('region_key', sa.String()),
                              sa.Column('category_key', sa.String()),
                              sa.Column('school_diff', sa.Integer()),
                              sa.Column('school_id', sa.Integer()),
                              sa.Column('hardk', sa.Float()))

history_table = sa.Table('history', metadata,
                         sa.Column('id', sa.Integer()),
                         sa.Column('vk_user_id', sa.Integer()),
                         sa.Column('time', sa.DateTime()),
                         sa.Column('category', sa.Integer()),
                         sa.Column('history_query', sa.String()))

categories_table = sa.Table('categories', metadata,
                            sa.Column('id', sa.Integer()),
                            sa.Column('name', sa.String()),
                            sa.Column('key', sa.String()))

cities_table = sa.Table('cities', metadata,
                        sa.Column('id', sa.Integer()),
                        sa.Column('name', sa.String()),
                        sa.Column('key', sa.String()))


@dataclass
class UserData:
    sirius_user_id: int
    categories_scores: Dict[str, float]


@dataclass
class ItemData:
    id_: int
    category: str
