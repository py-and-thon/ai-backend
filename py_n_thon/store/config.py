from dataclasses import dataclass


@dataclass
class Config:
    host: str
    port: str
    database_url: str
    redis_url: str
    models_path: str
