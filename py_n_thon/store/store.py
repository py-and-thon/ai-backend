import logging

from py_n_thon.store.config import Config

_logger = logging.getLogger(__name__)


class Store:
    def __init__(self, config: Config):
        from py_n_thon.store.accessors.events import EventsAccessor
        from py_n_thon.store.accessors.string_compare_model_file import StringCompareModelFileAccessor
        from py_n_thon.store.accessors.pg import PgAccessor
        from py_n_thon.store.accessors.string_compare import StringCompareAccessor
        from py_n_thon.store.accessors.cache import CacheAccessor
        from py_n_thon.store.accessors.item_data import ItemDataAccessor
        from py_n_thon.store.accessors.recommend import RecommendAccessor
        from py_n_thon.store.accessors.search import SearchAccessor
        from py_n_thon.store.accessors.user_data import UserDataAccessor

        self.pg = PgAccessor(self, config)
        self.string_compare = StringCompareAccessor(self)
        self.string_compare_model_file = StringCompareModelFileAccessor(self, config)
        self.search = SearchAccessor(self)
        self.recommend = RecommendAccessor(self)
        self.user_data = UserDataAccessor(self)
        self.item_data = ItemDataAccessor(self)
        self.cache = CacheAccessor(self)
        self.events = EventsAccessor(self)

        self.all = [self.pg, self.string_compare, self.string_compare_model_file, self.search,
                    self.recommend, self.user_data, self.cache, self.events, self.item_data]

    async def connect(self):
        for it in self.all:
            _logger.info(f'Connecting {it.__class__.__name__}')
            await it.connect()
        _logger.info(f'Connected all')

    async def disconnect(self):
        for it in self.all[::-1]:
            _logger.info(f'Disconnecting {it.__class__.__name__}')
            await it.disconnect()
        _logger.info(f'Disconnected all')
