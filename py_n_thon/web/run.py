import asyncio
import os

from sanic import Sanic

from py_n_thon.store.config import Config
from py_n_thon.store.store import Store
from py_n_thon.web.routes import api_routes


def main(config: Config) -> None:
    loop = asyncio.get_event_loop()

    # store
    store = Store(config)
    loop.run_until_complete(store.connect())

    # web
    web_app = Sanic()
    web_app.store = store

    web_app.blueprint(api_routes)

    server = web_app.create_server(host=config.host, port=config.port, debug=True)
    asyncio.ensure_future(server)
    loop.run_forever()


def parse_args() -> Config:
    try:
        host = os.getenv('HOST')
        if host is None:
            host = '127.0.0.1'

        port = os.getenv('PORT')
        if port is None:
            port = 5000
        port = int(port)

        database_url = os.getenv('DATABASE_URL')
        if database_url is None:
            database_url = 'postgres://localhost/vkhack'

        redis_url = os.getenv('REDIS_URL')
        if redis_url is None:
            redis_url = 'localhost'

        models_path = os.getenv('MODELS_PATH')
        if models_path is None:
            models_path = '.'
    except ValueError as e:
        print(e)
        print()
        print('USAGE: python3 ./run.py')
        print('Env: APP_ENV')
        print('     [HOST]')
        print('     [PORT]')
        print('     DATABASE_URL')
        print('     REDIS_URL')
        print('     MODELS_PATH')
        exit(1)

    # noinspection PyUnboundLocalVariable
    return Config(host=host, port=port,
                  database_url=database_url, redis_url=redis_url, models_path=models_path)


if __name__ == '__main__':
    main(parse_args())
