from sanic import Blueprint
from sanic.exceptions import abort
from sanic.request import Request

api_routes = Blueprint('api')


@api_routes.get('/api/search')
async def handle_search(req: Request):
    vk_user_id = req.args.get('vk_user_id')
    if vk_user_id is None:
        abort(400, 'required vk_user_id')
    sirius_user_id = req.args.get('sirius_user_id')
    if sirius_user_id is None:
        abort(400, 'required sirius_user_id')
    search_query = req.args.get('q')
    if search_query is None:
        abort(400, 'required q')

    events = await req.app.store.events.search(search_query, sirius_user_id)

    return {
        'results': [
            {
                'name': it['name'],
                'description': it['description'],
                'city': it['city'],
                'image_url': it['image_url'],
                'url': it['url'],
                'category': it['category']
            } for it in events
        ]
    }


@api_routes.get('/api/events')
async def handle_events(req: Request):
    vk_user_id = req.args.get('vk_user_id')
    if vk_user_id is None:
        abort(400, 'required vk_user_id')
    sirius_user_id = req.args.get('sirius_user_id')
    if sirius_user_id is None:
        abort(400, 'required sirius_user_id')

    sections = await req.app.store.events.list(sirius_user_id)

    return {
        'sections': [
            {
                'name': section_name,
                'events': [
                    {
                        'name': it['name'],
                        'description': it['description'],
                        'city': it['city'],
                        'image_url': it['image_url'],
                        'url': it['url'],
                        'category': it['category']
                    } for it in section_events
                ]
            } for section_name, section_events in sections.items()
        ]
    }


@api_routes.get('/api/tips')
async def handle_tips(req: Request):
    vk_user_id = req.args.get('vk_user_id')
    if vk_user_id is None:
        abort(400, 'required vk_user_id')
    sirius_user_id = req.args.get('sirius_user_id')
    if sirius_user_id is None:
        abort(400, 'required sirius_user_id')

    tips = await req.app.store.events.list_tips(sirius_user_id)

    return {
        'tips': tips
    }
